# Subprocess Mock

_Mock objects for the standard library’s subprocess module_


## Abstract

A simple-to-use approach to mocking subprocess calls
with the goal to simplify unit testing of wrapper modules.


## Installation

subprocess-mock is available on PyPI: <https://pypi.org/project/subprocess-mock/>

```text
pip install subprocess-mock
```

Installation in a virtual environment is strongly recommended.


## Example Usage

Use the functions defined in the **subprocess\_mock.functions** module
(and also exposed in the **subprocess_mock** namespace) to patch
subprocess module functions in unit tests.


```python
>>> import pathlib
>>> import subprocess
>>> import subprocess_mock
>>> from unittest.mock import patch
>>>
>>> new_file = pathlib.Path("testfile.txt")
>>> new_file.exists()
False
>>> # Test: call a process with a mock.patched subprocess.run
>>> # No Process is called,
>>> # but a subprocess.CompletedProcess instance is returned.
>>>
>>> with patch("subprocess.run", new=subprocess_mock.run):
...     run_result = subprocess.run(["touch", str(new_file)])
...
>>> run_result
CompletedProcess(args=['touch', 'testfile.txt'], returncode=0)
>>> new_file.exists()
False
>>>
>>> # Counter-test: call the process without patching subprocess.run
>>> # The process is called with normal effects.
>>>
>>> run_result = subprocess.run(["touch", str(new_file)])
>>> new_file.exists()
True
>>>
```


## The Orchestrator() class

_preloads programs and captures results_

Use the **subprocess\_mock.Orchestrator()** class
to preload miniature programs consisting of
steps defined as subclasses of **subprocess\_mock.child.Step**.

These steps will be executed sequentially as the next mini program
if the command line is matched.
Currently, the following classes are available:

+ **Filter(** _function_: `Callable`, _binary_=`False` **)** reads data from standard input,
  filters it through _function_ which should take exactly one argument.
  The argument type depends on the mode: `str` in text mode (default),
  or `bytes` if _binary_ is set `True`. The return value can be `bytes` or `str`,
  necessary conversions will be done automatically.
* **SetReturncode(** _returncode_: `int` **)** sets _returncode_ as the mocked subprocess’ returncode
* **Sleep(** _seconds_: `float` **)** pauses execution for the given amount of seconds
* **WriteError(** _message_: `Union[bytes, str]` **)** writes _message_ to standard error
* **WriteOutput(** _message_: `Union[bytes, str]` **)** writes _message_ to standard output


!!! tip "Remember redirecting stdout/stderr to capture output"

    If you use a step that writes to standard output or standard error,
    you should also redirect that stream, else your output will go to
    **sys.stdout** or **sys.stderr**
    (which _might_ be a legitimate use case as well).


```python
>>> import subprocess
>>> from subprocess_mock import Orchestrator, Program, WriteOutput
>>> from unittest.mock import patch
>>>
>>> orchestrator = Orchestrator()
>>> orchestrator.add_program("mock-helloworld", program=Program(WriteOutput("Hello World")))
>>> with patch("subprocess.run", new=orchestrator.run):
...     run_result = subprocess.run(["mock-helloworld"], stdout=subprocess.PIPE)
...
>>> run_result
CompletedProcess(args=['mock-helloworld'], returncode=0, stdout=b'Hello World')
>>>
>>> orchestrator.all_results
[(Program(WriteOutput('Hello World'),), CompletedProcess(args=['mock-helloworld'], returncode=0, stdout=b'Hello World'), {'stdout': -1})]
>>>
```