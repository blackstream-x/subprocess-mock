#!/bin/bash

# Quick code check

echo -e "\n=== mypy ===\n"
python3 -m mypy . || exit
echo -e "\n=== pylint (tests) ===\n--- ignoring duplicate-code ---\n"
PYTHONPATH=src python3 -m pylint --disable=duplicate-code tests || echo "--- ignored issues in test modules ---"
echo -e "\n=== pylint (src)  ===\n"
PYTHONPATH=src python3 -m pylint --disable=fixme --reports=y src || exit
echo -e "\n=== ruff ===\n"
ruff format --check || ruff format --diff
ruff check
